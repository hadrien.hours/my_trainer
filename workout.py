import csv
import logging
import numpy as np
import time

logger = logging.getLogger(__name__)


def countdown(t):
    """
    Print timer countdown
    code from https://stackoverflow.com/questions/25189554/countdown-clock-0105
    :param t:
    :return:
    """
    while t:
        mins, secs = divmod(t, 60)
        timeformat = '{:02d}:{:02d}'.format(mins, secs)
        print(timeformat, end='\r')
        time.sleep(1)
        t -= 1
    print("Time's up!\n\n")


def countdown_next(t, msg):
    """
    Print timer countdown with message
    :param t: counter value
    :param msg: message
    :return:
    """
    while t:
        mins, secs = divmod(t, 60)
        timeformat = '{:02d}:{:02d}'.format(mins, secs)
        print(msg, 'in', timeformat, 's', end='\r')
        time.sleep(1)
        t -= 1
    print("\n")


def print_exercise(name, nreps):
    """
    Method to print on screen name of the exercise and number of reps
    :param name: string
    :param nreps: int
    :return:
    """
    print("*****************")
    print("*{ex}:\t{n} reps".format(ex=name, n=nreps))
    print("*****************\n")


def pick_random_entry(root_folder, filename):
    """
    Pick a random entry from a csv file
    :param root_folder:
    :param filename:
    :return:
    """
    with open(root_folder+'/'+filename, 'r') as f:
        csv_reader = csv.reader(f, delimiter=',')
        d = {}
        for row in csv_reader:
            d[row[0].strip()] = int(row[1].strip())
    k = np.random.choice(list(d.keys()))
    return k, d[k]

class Set:
    """
    One set for a workout consists in 6 exercises
    - Push ups
    - Leg
    - Shoulder
    - Leg
    - Back
    - Arm

    Input parameters are:
    - rest time (int)
    - root folder (optional, string): Folder containing
        + pushup1.csv
        + leg1.csv
        + shoulder1.csv
        + leg2.csv
        + back1.csv
        + arm1.csv
    - each csv file in the form [exercise, nr_reps]
    """

    #
    list_file_names = ['pushup1.csv',
                       'leg1.csv',
                       'shoulder1.csv',
                       'leg2.csv',
                       'back1.csv',
                       'arm1.csv']

    def init_library(self):
        """
        Initialize library containing all possible exercises categories
        :return:
        """
        self.library = {}
        for name in [_.split('.')[0] for _ in Set.list_file_names]:
            self.library[name] = {}

    def __init__(self, rest_time, root_folder=None):
        """
        Initialize set
        :param rest_time: Rest time between each exercise
        :param root_folder: Optional, folder containing csv files with exercises
        """
        self.library = {}
        self.init_library()
        self.root_folder = root_folder
        self.rest_time = rest_time
        self.program = {'pushup1': None,
                        'leg1': None,
                        'shoulder1': None,
                        'leg2': None,
                        'back1': None,
                        'arm1': None}

    def build_library(self):
        """
        Populate library with list of possible exercises
        :return:
        """
        if self.root_folder is not None:
            for filename in self.list_file_names:
                filepath = self.root_folder + '/' + filename
                key = str(filename.split('.')[0])
                with open(filepath, 'r') as f:
                    csv_reader = csv.reader(f, delimiter=',')
                    for row in csv_reader:
                        try:
                            v = row[0].strip()
                            n = int(row[1].strip())
                        except ValueError:
                            logger.error('Wrong format for entry {}'.format(','.join(row)))
                        self.library[key][v] = n
        # TO DO implement method to enter by hand
        else:
            pass

    def random_populator(self):
        """
        Randomly pick one exercise for each category
        :return:
        """
        for k in list(self.library.keys()):
            ex = np.random.choice(list(self.library[k].keys()))
            self.program[k] = (ex, self.library[k][ex])

    def populate(self):
        """
        Allow user to pick exercise for each category
        :return:
        """
        for k in list(self.library.keys()):
            list_exs = list(self.library[k].keys())
            print('Please pick exercise for category:\t{cat}'.format(cat=k))
            for i, v in enumerate(list_exs):
                print('\t[{0:3d}]: {exo}'.format(i, ex0=v))
            index = input('Your choice:\t')
            if index >= len(list_exs):
                logger.error("Choice out of bound, max {}".format(len(list_exs)))
                return -1
            exo = list_exs[index]
            self.library[k] = {exo, self.library[k][exo]}

    def print_program(self):
        print("\n**************************")
        print("*\tPROGRAM SUMMARY\t*")
        print("**************************")
        for k in self.program.keys():
            ex = self.program[k][0]
            nreps = self.program[k][1]
            print('{cat}:\t{exo} ({n} reps)'.format(cat=k, exo=ex, n=nreps))
        print("**************************")

    def print_set(self):
        """
        Print the set with rest times
        :return:
        """
        for entry in self.program.items():
            print_exercise(entry[1][0], entry[1][1])
            input("Press enter once done")
            print("\n\t\tRest time {}\n".format(self.rest_time))
            countdown(self.rest_time)

    def print_set_next(self):
        """
         Print the set with rest times
         :return:
         """
        list_cat = list(self.program.keys())
        for i,key in enumerate(list_cat):
            entry = self.program[key]
            print_exercise(entry[0], entry[1])
            input("Press enter once done")
            print("\n\t\tRest time {}\n".format(self.rest_time))
            if i+1 < len(list_cat):
                next_entry = self.program[list_cat[i+1]]
                msg = "{exo} ({n} reps)".format(exo=next_entry[0], n=next_entry[1])
                countdown_next(self.rest_time, msg)
            else:
                countdown(self.rest_time)


class Workout:
    """
    Sequence of warm up, sets, core and cool down
    inputs:
        number of sets (int)
        rest time (int)
        root folder (string) containing csv files with exercises per categories
    """

    def __init__(self, n_sets, rest_time, root_folder=None):
        """
        Initialize workout object
        :param n_sets: number of sets
        :param rest_time: time of rest between sets
        :param root_folder: optional, folder storing exercise list
        """
        self.root_folder = root_folder
        self.warmup = None
        self.n_sets = n_sets
        self.rest_time = rest_time
        self.base_set = Set(rest_time, root_folder)
        self.cool_down = None

    def populate_workout(self):
        """
        Pick warm up, basic set and cool down exercises
        :return:
        """
        if hasattr(self, 'root_folder'):
            self.warmup = pick_random_entry(self.root_folder, 'warmup1.csv')
            self.base_set.build_library()
            self.base_set.random_populator()
            self.cool_down = pick_random_entry(self.root_folder,'cooldown1.csv')
        # TODO
        else:
            logger.info("Not implemented")
            pass

    def print_init_message(self):
        """
        Print summary of created workout
        :return:
        """
        print("\n\n******************************************************************\n")
        print("Welcome to your workout generator")
        print("Beta version: bug report to {}".format('No defined'))
        print("Workout consists of:")
        print("\tA warm up\n\t{n} set(s) of 6 exercises\n\tA cool down\n\tThe rest time is {t}s"
              .format(n=self.n_sets, t=self.rest_time))
        print("\n**************************")
        print("*\tWARM UP\t\t*")
        print("**************************")
        print("Warmup:\t{exo} ({n} reps)".format(exo=self.warmup[0], n=self.warmup[1]))
        print("**************************")
        self.base_set.print_program()
        print("\n**************************")
        print("*\tCOOL DOWN\t*")
        print("**************************")
        print("Cooldown:\t{exo} ({n} reps)".format(exo=self.cool_down[0], n=self.cool_down[1]))
        print("**************************")
        print("\n******************************************************************\n")
        input("Press enter when ready to start:\n\n")
        print("\t\tLETS GO!\n\n")

    def print_workout(self):
        """
        Main function displaying exercises and rest times
        :return:
        """
        start = time.time()
        self.print_init_message()
        print("\n****************************************")
        print("*\t\t WARM UP\t\t*")
        print("****************************************\n")
        print_exercise(self.warmup[0], self.warmup[1])
        input("Press enter when done")
        print("\n\t\tRest time {}\n".format(self.rest_time))
        countdown(self.rest_time)
        for set_idx in range(1, self.n_sets+1):
            print("\n****************************************")
            print("*\t\t SET {}\t\t*".format(set_idx))
            print("****************************************\n")
            input("Press enter when ready:\n")
            self.base_set.print_set_next()
        print("\n****************************************")
        print("*\t\t COOL DOWN\t\t*")
        print("****************************************\n")
        print_exercise(self.cool_down[0], self.cool_down[1])
        input("Press enter when done")
        end = time.time()
        time_last = int(np.round(end - start))
        mins, secs = divmod(time_last, 60)
        print("\n****************************************")
        print("*\t\t WELL DONE\t\t*")
        print("*\t\t Time: {:02d}:{:02d}".format(mins, secs))
        print("****************************************\n")

