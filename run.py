from workout import Workout
from argparse import ArgumentParser


def main():
    parser = ArgumentParser(description="Simple Workout generator")
    parser.add_argument('-d', '--directory', type=str, default='exercises.dir',
                        help='directory storing exercises and reps as csv files')
    parser.add_argument('-n', '--nsets', type=int, default=4,
                        help='number of sets')
    parser.add_argument('-r', '--rest', default=30, type=int,
                        help='rest time between exercises')

    args = parser.parse_args()
    W = Workout(args.nsets, args.rest, args.directory)
    W.populate_workout()
    W.print_workout()


if __name__ == '__main__':
    main()