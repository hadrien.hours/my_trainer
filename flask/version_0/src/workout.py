import csv, logging, time
import numpy as np
import sqlalchemy as db
import pandas as pd

logger = logging.getLogger(__name__)


def countdown_next(t, msg):
    """
    Print timer countdown with message
    :param t: counter value
    :param msg: message
    :return:
    """
    pass


def print_exercise(name, nreps):
    """
    Method to print on screen name of the exercise and number of reps
    :param name: string
    :param nreps: int
    :return:
    """
    pass


def create_connection_db(db_path):
    """
    Connect to sql database
    https://towardsdatascience.com/sqlalchemy-python-tutorial-79a577141a91
    :param db_path: path to db
    :return: engine, metadata, connection
    """
    engine = db.create_engine('sqlite:///{}'.format(db_path))
    connection = engine.connect()
    metadata = db.MetaData()
    return engine, connection, metadata


def query_cat_entry(engine, metadata, connection, tablename, level, cat):
    """
    Pick a entries from a given category and level
    :param engine: sqlalchemy engine
    :param metadata: sqlalchemy metadata
    :param connection: sqlalchemy connection
    :param tablename: name exercise table
    :param level: easy, intermediate or hard
    :param cat: category in ['leg1','leg2','arm1','back1','shoulder1',
                            'warmup1','cooldown1','pushup1']
    :return result:
    """
    table = db.Table(tablename, metadata, autoload=True, autoload_with=engine)
    query = db.select([table]).where(db.and_(table.columns.category == cat,\
                                             table.columns.level == level))
    result = connection.execute(query)
    res_data = result.fetchall()
    return res_data


def convert_pd(pd_in):
    """
    Convert a dataframe into exercise_struct dict
    :param pd_in: input dataframe
    :return: one or list of dicts
    """
    list_cols = list(pd_in.columns)
    list_keys = list(Set.exercise_struct.keys())
    # check needed values are present
    diff = [_ for _ in list_keys if _ not in list_cols]
    if len(diff) > 0:
        logger.error('Missing entries from DB extract {}'.format(','.join(diff)))
        return None
    list_entries = []
    for idx, row in pd_in.iterrows():
        d = Set.exercise_struct.copy()
        for k in d.keys():
            d[k] = row[k]
            list_entries.append(d)
    return list_entries


def pick_random_entry(res_data):
    """
    From result of a query, return a random, formatted entry
    :param res_data: result of sqlalchemy query
    :return: one exercise
    """
    df = pd.DataFrame(res_data)
    df.columns = res_data[0].keys()
    entries = convert_pd(df.sample())
    return entries[0]


def get_cat_entries(res_data):
    """
     convert query result into exercise format
     :param res_data: result of sqlalchemy query
     :return exercises:
     """
    df = pd.DataFrame(res_data)
    return convert_pd(df)


class Set:
    """
    One set for a workout consists in 6 exercises
    - Push ups
    - Leg
    - Shoulder
    - Leg
    - Back
    - Arm

    Input parameters are:
    - db path: path to sql db
    - rest time (int)
    """

    exercise_struct = {'name': '', 'reps': 0, 'metric': '', 'description': ''}

    def __init__(self, db_path, tablename, level, rest_time):
        """
        Initialize set
        :param db_path: path to sql db with exercises
        :param tablename: table containing exercise data
        :param level: level of difficulty (easy, medium, hard)
        :param rest_time: Rest time between each exercise
        """
        engine, connection, metadata = create_connection_db(db_path)
        self.engine = engine
        self.metadata = metadata
        self.connection = connection
        self.tablename = tablename
        self.level = level
        self.rest_time = rest_time
        self.exercises = {'pushup1': None,
                          'leg1': None,
                          'shoulder1': None,
                          'leg2': None,
                          'back1': None,
                          'arm1': None}

    def random_populator(self):
        """
        Randomly pick one exercise for each category
        :return:
        """
        for cat in list(self.exercises.keys()):
            result_query = query_cat_entry(self.engine, self.metadata, self.connection,
                                           self.tablename, self.level, cat)
            exercise = pick_random_entry(result_query)
            self.exercises[cat] = exercise

    def populate(self):
        """
        Allow user to pick exercise for each category
        :return:
        """
        for cat in list(self.exercises.keys()):
            result_query = query_cat_entry(self.engine, self.metadata, self.connection,
                                           self.tablename, self.level, cat)
            list_exs = self.get_cat_entries(result_query)
            print('Please pick exercise for category:\t{cat}'.format(cat=cat))
            for i, v in enumerate(list_exs):
                print('\t[{0:3d}]: {exo}'.format(i, ex0=v['name']))
            index = input('Your choice:\t')
            if index >= len(list_exs):
                logger.error("Choice out of bound, max {}".format(len(list_exs)))
                return -1
            exo = list_exs[index]
            self.exercises[cat] = exo


class Workout:
    """
    Sequence of warm up, sets, core and cool down
    inputs:
        number of sets (int)
        rest time (int)
        root folder (string) containing csv files with exercises per categories
    """

    def __init__(self, db_path, tablename, level, n_sets, rest_time):
        """
        Initialize workout object
        :param db_path: path to sql database containing exercises
        :param tablename: name of exercise table
        :param level: easy, medium or hard
        :param n_sets: number of sets
        :param rest_time: time of rest between sets
        """
        engine, connection, metadata = create_connection_db(db_path)
        self.engine = engine
        self.connection = connection
        self.metadata = metadata
        self.tablename = tablename
        self.level = level
        self.warmup = None
        self.n_sets = n_sets
        self.rest_time = rest_time
        self.base_set = Set(db_path, self.tablename, self.level, self.rest_time)
        self.core = None
        self.cool_down = None

    def populate_workout(self):
        """
        Pick warm up, basic set and cool down exercises
        :return:
        """
        warmup_entries = query_cat_entry(self.engine, self.metadata, self.connection,
                                         self.tablename, self.level, 'warmup1')
        if len(warmup_entries) == 0:
            logger.error('No match found for {} at level {}'.format('warmup1',self.level))
            return None
        cooldown_entries = query_cat_entry(self.engine, self.metadata, self.connection,
                                           self.tablename, self.level, 'cooldown1')
        if len(cooldown_entries) == 0:
            logger.error('No match found for {} at level {}'.format('cooldown1',self.level))
            return None
        core_entries = query_cat_entry(self.engine, self.metadata, self.connection,
                                       self.tablename, self.level, 'core1')
        if len(core_entries) == 0:
            logger.error('No match found for {} at level {}'.format('core1',self.level))
            return None
        self.warmup = pick_random_entry(warmup_entries)
        self.cool_down = pick_random_entry(cooldown_entries)
        self.core = pick_random_entry(core_entries)
        self.base_set.random_populator()
