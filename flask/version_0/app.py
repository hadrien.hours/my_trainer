from flask import Flask, render_template, url_for, request, redirect, flash, send_from_directory
from flask_sqlalchemy import SQLAlchemy
import os, time, sys
from werkzeug.utils import secure_filename
import csv, pandas as pd, numpy as np

sys.path.append('src')
from workout import Workout

DBNAME = 'exercises.db'
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///{}'.format(DBNAME)
db = SQLAlchemy(app)

ALLOWED_EXTENSIONS = ['csv']
UPLOAD_FOLDER = 'data'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024
app.secret_key = str(np.random.randint(10000))

ALLOWED_CATEGORIES = ['pushup1','leg1', 'leg2', 'arm1', 'shoulder1', 'back1', 'core1', 'warmup1', 'cooldown1']
ALLOWED_LEVELS = ['easy', 'medium', 'hard']
ALLOWED_METRICS = ['reps', 'seconds']


def allowed_file(filename):
    """
    Check file extension
    :param filename:
    :return:
    """
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def check_file_format(filename):
    """
    Make sure file contains
    :param filename:
    :return:
    """
    with open(filename, 'r') as f:
        pd1 = pd.read_csv(f)
        list_cols = [_ for _ in Exercises.__table__.columns.keys() if _ != 'id']
        pd1.columns = [_.strip() for _ in pd1.columns]
        diff = [_ for _ in list_cols if _ not in pd1.columns]
        if len(diff) != 0:
            flash('Missing columns {}'.format(','.join(diff)))
            return False
    return True


def create_record(row):
    """
    From a pandas series / row. Create a db input record
    Control format and value
    :param row:
    :return: db record
    """
    cat = row['category'].strip()
    if cat not in ALLOWED_CATEGORIES:
        return None
    level = row['level'] or 'intermediate'
    level = level.strip()
    if level not in ALLOWED_LEVELS:
        return None
    metric = row['metric'] or 'reps'
    metric = metric.strip()
    if metric not in ALLOWED_METRICS:
        return None
    reps = row['reps']
    try:
        reps = int(reps)
    except ValueError:
        return None
    record = Exercises(category=cat,
                       level=level,
                       name=row['name'],
                       reps=reps,
                       metric=metric,
                       description=row['description']
                       )
    return record


def populate_db(filepath, db):
    """
    Try populating database from data read from csv file
    :param filepath:
    :param db:
    :return:
    """
    if not check_file_format(filepath):
        return 'Wrong file format'
    with open(filepath, 'r') as f:
        pd1 = pd.read_csv(f)
        pd1.columns = [_.strip() for _ in pd1.columns]
        try:
            for index, row in pd1.iterrows():
                record = create_record(row)
                if record:
                    db.session.add(record)
            db.session.commit()
            flash('Success in uploading file')
        except:
            flash("Issue in uploading file")
            db.session.rollback()


class Exercises(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    category = db.Column(db.String(100), nullable=False)
    name = db.Column(db.String(100), nullable=False)
    reps = db.Column(db.Integer, nullable=False)
    metric = db.Column(db.String(10), nullable=True)
    description = db.Column(db.String(400), nullable=True)
    level = db.Column(db.Integer, nullable=True)

    def __repr__(self):
        return '<Exercises %r>' % self.id


@app.route('/generate/', methods=['POST', 'GET'])
def generate():
    if request.method == 'POST':
        n_sets = request.form['n_sets']
        rest_time = request.form['rest_time']
        level = request.form['level']
        workout = Workout(db_path=DBNAME,
                          tablename='Exercises',
                          level=level,
                          n_sets=n_sets,
                          rest_time=rest_time)
        workout.populate_workout()
        return render_template('render_program.html', workout=workout)
        # return '{}\n{}\n{}\n{}\n'.format(workout.warmup, workout.base_set, workout.core, workout.cool_down)
    else:
        return render_template('generate_program.html')


@app.route('/add_exercise/', methods=['POST', 'GET'])
def add_exercise():
    if request.method == 'POST':
        cat = request.form['cat']
        name = request.form['name']
        nreps = request.form['reps']
        try:
            n = int(nreps)
        except ValueError:
            n = None
        metric = request.form['metric']
        desc = request.form['desc']
        level = request.form['level']
        exercise = Exercises(category=cat,
                             name=name,
                             reps=nreps,
                             metric=metric,
                             description=desc,
                             level=level)
        try:
            db.session.add(exercise)
            db.session.commit()
            return redirect('/')
        except:
            return 'There was an issue adding the exercise'
    else:
        return render_template('add_exercise.html')


@app.route('/add_exercise/upload_file/', methods=['GET', 'POST'])
def upload_file():
    # https://flask.palletsprojects.com/en/1.1.x/patterns/fileuploads/
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file part')
            redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(filepath)
            start = time.time()
            flag = os.path.exists(filepath)
            delta_t = 0
            while (not flag) and (delta_t < 30):
                flash('Waiting for file upload')
                time.sleep(5)
                delta_t = time.time() - start
                flag = os.path.exists(filepath)
            populate_db(filepath, db)
            os.remove(filepath)
            return redirect('/')
    else:
        return render_template('upload_file.html')


@app.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'], filename)


@app.route('/list_exercises/')
def list_exercises():
    if 'back' in request.form:
        return redirect('/')
    else:
        exercises = Exercises.query.order_by(Exercises.level, Exercises.category).all()
        return render_template('list_exercises.html', exercises=exercises)


@app.route('/list_exercises/delete/<int:id>')
def delete(id):
    exo_to_delete = Exercises.query.get_or_404(id)
    try:
        db.session.delete(exo_to_delete)
        db.session.commit()
        return redirect('/list_exercises')
    except:
        return 'There was a problem deleting that exercise'


@app.route('/list_exercises/update/<int:id>', methods=['GET', 'POST'])
def update(id):
    exo = Exercises.query.get_or_404(id)

    if request.method == 'POST':
        exo.category = request.form['category']
        exo.level = request.form['level']
        exo.name = request.form['name']
        exo.reps = request.form['reps']
        exo.metric = request.form['metric']
        exo.description = request.form['description']
        try:
            db.session.commit()
            return redirect('/')
        except:
            return 'There was an issue updating your exercise'
    else:
        return render_template('update.html', exercise=exo)

@app.route('/list_exercises/reset/')
def reset():
    try:
        db.session.query(Exercises).delete()
        db.session.commit()
        return redirect('/')
    except:
        db.session.rollback()
        return 'There was a problem reset exercises'

@app.route('/modify_exercises')
def modify_exercise():
    render_template('modify_exercises.html')


@app.route('/')
def index():
    return render_template('index.html')


if __name__ == '__main__':
    app.run(debug=True)
