# Infos

Author: Hadrien Hours      
Last update: 2021-02-08    
Description: Automate the generation of pseudo random workouts

# Description

# Usage
```python
python3 run.py
```

If you want to change rest time (default is 30s) or number of sets (default is 4)
```python
python3 run.py -r new_rest_time -n new_number_sets
```
